Facet API Inline Links
-------
A Facet API widget that displays facet items as inline links.

Widget allows you to organize the output facet elements (reference filter) in 
one line, that is, using a wrapper DIV > SPAN, instead of UL > LI (both in 
standard widgets).

Also, facet's counter has been imposed from tag A (for greater beauty).


Standard usage scenario
--------------------------------------------------------------------------------
1. Download module, upload to server (./sites/all/modules).
2. Go to administration page (admin/modules) and enable module.
3. Go to facet's display settings 
(admin/config/search/search_api/index/[YOUR_SEARCH_INDEX]/facets) and click 
«Show Widget». In open select-box choose «Inline links». Thats it!


Credits / contact
--------------------------------------------------------------------------------
Currently maintained by Victor Shostak [1].

Ongoing development is sponsored by IA Central marketing [2] and Blizzy's 
Blog [3].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://github.com/enjoyiacm/facetapi_inline_links/issues


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/enjoyiacm
2: http://centralmarketing.ru
3: http://blizzy.ru
